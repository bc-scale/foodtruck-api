<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BookingRepository;
use App\Validator as CustomAssert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[
    ORM\Entity(repositoryClass: BookingRepository::class),
    ApiResource
]
class Booking
{
    #[
        ORM\Id,
        ORM\GeneratedValue,
        ORM\Column(type: 'integer')
    ]
    private ?int $id = 0;

    #[
        ORM\Column(type: 'date'),
        Assert\NotBlank,
        Assert\GreaterThan('today 24:00'),
        CustomAssert\BookingDateConstraint
    ]
    private ?\DateTimeInterface $bookingDate;

    #[
        ORM\Column(type: 'string', length: 25),
        Assert\NotBlank,
        CustomAssert\BookingTruckConstraint
    ]
    private ?string $truckNumberplate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBookingDate(): ?\DateTimeInterface
    {
        return $this->bookingDate;
    }

    public function setBookingDate(\DateTimeInterface $bookingDate): self
    {
        $this->bookingDate = $bookingDate;

        return $this;
    }

    public function getTruckNumberplate(): ?string
    {
        return $this->truckNumberplate;
    }

    public function setTruckNumberplate(string $truckNumberplate): self
    {
        $this->truckNumberplate = $truckNumberplate;

        return $this;
    }
}
